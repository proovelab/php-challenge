* [PHP 5.x](http://php.net/)

* [PostgreSQL 9.x](https://www.postgresql.org)

* [Phalcon framework 2.x](http://phalconphp.com) or [Laravel 5.x](https://laravel.com)

## Help to installation [debian 8.x](http://cdimage.debian.org/debian-cd/8.5.0/amd64/iso-cd/debian-8.5.0-amd64-netinst.iso)

```
$ sudo apt-get upgrade
$ sudo apt-get update
$ sudo apt-get -y install ssh sudo make automake autoconf
$ sudo apt-get -y install wget git-core curl build-essential openssl libssl-dev
$ sudo apt-get -y install python-software-properties
$ sudo apt-get -y install apt-transport-https
```

### PostgreSQL

```
$ apt-get -y install postgresql
$ sudo -u postgres psql
postgres=# CREATE USER "username" WITH PASSWORD 'password';
postgres=# CREATE DATABASE "db_name" WITH OWNER "username";
postgres=# \quit
```

Where:
* *username* - your user name
* *password* - user's password
* *db_name* - database name

### nginx & php installation

```
$ sudo apt-get -y install nginx
$ sudo apt-get -y install php5 php5-cli php5-dev libpcre3-dev gcc php5-curl php5-gd php5-imagick php5-mcrypt libmp-dev php5gmp

$ sudo nano /etc/php5/fpm/pool.d/www.conf
```

Need to change the following:
```
#!txt
listen = 127.0.0.1:9000
```

Reset server

```
$ sudo service php5-fpm restart
```

```
$ mkdir -p /var/project_dir/
$ chown -R www-data:www-data /var/dir_name
$ chmod -R 755 /var/project_dir
```

Where *project_dir* - is your project name

### To install Phalcon

```
$ cd ~
$ git clone --depth=1 git://github.com/phalcon/cphalcon.git
$ cd cphalcon/build
$ sudo ./install
$ service php5-fpm restart
$ cd ../..
$ rm -rf cphalcon
$ echo "extension=phalcon.so" > /etc/php5/mods-available/phalcon.ini
$ php5enmod phalcon
$ service php5-fpm restart
```