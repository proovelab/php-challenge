#Goal

Realise API with JSON body. show tech stack knowledge.

#XHR

Xhr directory has got JSON stub files as examples.

All requests must run by POST, requests data have to pass as HTTP body as JSON.

Need to realise classes and handlers to process requests, all business-logic have to be divided into layers (Routing, DI, DAO, Entity Collections, Requests processors, Services, Response renders).

Need to use the follow tech stack:

* PHP 5.x
* PostgreSOL 9.x
* Phalcon framework 2.x or Laravel 5.x

All interfaces are described in [API specifications](API specifications.md).

You need to create git repo on any public service and develop project with it.