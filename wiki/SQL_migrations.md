SQL structure and all necessary data must be presented as php migration scripts.

Our recommendation is using one of the following tools:

* [Phinx](https://phinx.org)
* [Phalcon migrations](https://docs.phalconphp.com/en/latest/reference/migrations.html)
* [Laravel migrations](https://laravel.com/docs/5.2/migrations)
