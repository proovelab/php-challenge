**Method** POST

**Request** /friend/add

**Headers**

```
Token: zmo38eqoz38qo8mnxi47ybce5inw48x3mqz
```

**Request body**

```
#!json
{
  "user_id": 123
}
```

**Parameters**

* *user_id* - user identifier which want to add to friends

**Errors**

* *TOKEN_REQUIRED*
* *BAD_TOKEN*
* *USER_ID_REQUIRED*

**Access levels**

* *Authorised users*

**Response**

```
#!json
null
```