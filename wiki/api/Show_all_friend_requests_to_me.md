**Method** POST

**Request** /friend/outbox

**Headers**

```
Token: zmo38eqoz38qo8mnxi47ybce5inw48x3mqz
```

**Request body** without body

**Errors**

* *TOKEN_REQUIRED*
* *BAD_TOKEN*

**Access levels**

* *Authorised users*

**Response**

```
#!json
{
  "users": [
    {
      "id": 3,
      "name": "Helen",
      "surname": "Potapova",
      "nickname": "helen",
      "registered": "08/06/2016"
    },
    {
      "id": 4,
      "name": "Marina",
      "surname": "Petrova",
      "nickname": "petrova",
      "registered": "10/05/2016"
    }
  ]
}
```

**Parameters**

* *users > id* - user's id
* *users > name* - user's name
* *users > surname* - user's surname
* *users > nickname* - user's nickname
* *users > registered* - user's registered timestamp
