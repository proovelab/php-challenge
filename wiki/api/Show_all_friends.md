**Method** POST

**Request** /friend/all

**Headers**

```
Token: zmo38eqoz38qo8mnxi47ybce5inw48x3mqz
```

**Request body** without body

**Errors**

* *TOKEN_REQUIRED*
* *BAD_TOKEN*

**Access levels**

* *Authorised users*

**Response**

```
#!json
{
  "users": [
    {
      "id": 5,
      "name": "Vladimir",
      "surname": "Konstantinov",
      "nickname": "kons",
      "registered": "15/02/2016"
    },
    {
      "id": 6,
      "name": "Konstantin",
      "surname": "Petrov",
      "nickname": "petr",
      "registered": "14/06/2016"
    }
  ]
}
```

**Parameters**

* *users > id* - user's id
* *users > name* - user's name
* *users > surname* - user's surname
* *users > nickname* - user's nickname
* *users > registered* - user's registered timestamp
