**Method** POST

**Request** /profile

**Headers**

```
Token: zmo38eqoz38qo8mnxi47ybce5inw48x3mqz
```

**Request body**

```
#!json
{
  "user_id": 123
}
```

**Parameters**

* *user_id* - user identifier which want to add to friends

**Errors**

* *TOKEN_REQUIRED*
* *BAD_TOKEN*
* *USER_ID_REQUIRED*

**Access levels**

* *Authorised users*

**Response**

```
#!json
{
  "user": {
    "id": 1,
    "name": "Ivan",
    "surname": "Ivanov",
    "nickname": "Ivan",
    "registered": "13/06/2016"
  }
}
```

**Parameters**

* *user > id* - user's id
* *user > name* - user's name
* *user > surname* - user's surname
* *user > nickname* - user's nickname
* *user > registered* - user's registered timestamp
