**Method** POST

**Request** /search

**Headers**

```
Token: zmo38eqoz38qo8mnxi47ybce5inw48x3mqz
```

**Request body**

```
#!json
{
  "query": "Ivanov"
}
```

**Parameters**

* *query* - search query

**Errors**

* *TOKEN_REQUIRED*
* *BAD_TOKEN*
* *QUERY_REQUIRED*

**Access levels**

* *Authorised users*

**Response**

```
#!json
{
  "users": [
    {
      "id": 1,
      "name": "Ivan",
      "surname": "Ivanov",
      "nickname": "Ivan",
      "registered": "13/06/2016"
    },
    {
      "id": 2,
      "name": "Sergey",
      "surname": "Ivanov",
      "nickname": "serg",
      "registered": "10/06/2016"
    }
  ]
}
```

**Parameters**

* *users > id* - user's id
* *users > name* - user's name
* *users > surname* - user's surname
* *users > nickname* - user's nickname
* *users > registered* - user's registered timestamp
