**Method** POST

**Request** /login

**Headers** without additional

**Request body**

```
#!json
{
  "username": "Ivan",
  "password": "password"
}
```

**Parameters**

* *username* - user name
* *password* - user password


**Errors**

* *USERNAME_REQUIRED*
* *PASSWORD_REQUIRED*
* *BAD_USERNAME* 
* *BAD_PASSWORD*
* *BAD_AUTH* 

**Access levels**

* *Guest only*

**Response**

```
#!json
{
  "token": "zmo38eqoz38qo8mnxi47ybce5inw48x3mqz"
}
```

**Parameters**

* *token* - session token