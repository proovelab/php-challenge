# General requirements

* [SQL injection](https://en.wikipedia.org/wiki/SQL_injection) protection
* [File inclusion vulnerability](https://en.wikipedia.org/wiki/File_inclusion_vulnerability) protection
* [Cross-site scripting](https://en.wikipedia.org/wiki/Cross-site_scripting) protection
* [Shell injection](https://en.wikipedia.org/wiki/Shell_injection) protection

# Routing and versioning

All request paths have to have the following structure:

```
/v1/object/action
```

Where:

* *v1* - version number
* *object* - some object in some context (use [DDD](https://en.wikipedia.org/wiki/Domain-driven_design))
* *action* - some action on the object

All data have to be in request body.

# Validation of request data

Must be realised the following steps:

1. Check existence if the field is required
2. Check by type (check if expected string field is string type)
3. Check by format if is necessary (check if expected field with email is really email by [RegExp](https://en.wikipedia.org/wiki/Regular_expression) or others language tools)
4. Check by logic (check if required object id is located in database)
