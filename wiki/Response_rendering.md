# Response with error

Response example:

```
#!json
{
  "err": "ERROR_CODE"
}
```

Where *err* is string error code.

Code examples:

* *TOKEN_REQUIRED* - in the case, when request must contain session token
* *BAD_ID* - in the case, when request must contain id
* *BAD_EMAIL* - in the case, when request field contain bad email, example: mail@dimain@com

# Response with data

Response example:

```
#!json
{
  "user": {
    "id": 1,
    "name": "Ivan",
    "surname": "Ivanov",
    "nickname": "Ivan",
    "registered": "13/06/2016"
  }
}
```

NB Any object must be wrapped

# Response without data

Response example:

```
#!json
null
```

# Response with values

Response example:

```
#!json
{
  "token": "zmo38eqoz38qo8mnxi47ybce5inw48x3mqz"
}
```