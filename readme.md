#Goal
Realise API with JSON body, show tech stack knowledge.

Will be good if you:

* can find out mistakes in the specification
* offer reasoned improvements
* create unit or/and integration tests

You need to create git repo on any public service and develop project with it.

Frequency of comments will be rated.

You can spend less that 8 hours.

# Description
Full description is located in project's [wiki](./wiki/Requirements.md).